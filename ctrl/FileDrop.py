import wx

class FileDrop(wx.FileDropTarget):

    def __init__(self, window):

        wx.FileDropTarget.__init__(self)
        self.fxnotebook = window
        self.app=wx.GetApp()

    def OnDropFiles(self, x, y, filenames):
        for name in filenames:
            self.app.frame.addTab(name)       

        return True