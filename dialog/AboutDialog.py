import wx 

class AboutDialog(wx.Dialog):
    '''
    弹出窗
    '''
    def __init__(self,parent,id):

        self.app=wx.GetApp()
        wx.Dialog.__init__(self,parent,id,'关于',size=(200,230))

        self.sizer1=wx.BoxSizer(wx.VERTICAL)
        self.sizer1.Add(wx.StaticText(self,-1,"wxedit"),0,wx.ALIGN_CENTER_HORIZONTAL|wx.TOP,border=20)
        self.sizer1.Add(wx.StaticText(self,-1,"时间：2022年02月05日"),0,wx.ALIGN_CENTER_HORIZONTAL|wx.TOP,border=10)
        self.sizer1.Add(wx.StaticText(self,-1,"Version 1.0"),0,wx.ALIGN_CENTER_HORIZONTAL|wx.TOP,border=10)
        self.sizer1.Add(wx.StaticText(self,-1,"Author : wangzonghui"),0,wx.ALIGN_CENTER_HORIZONTAL|wx.TOP,border=10)

        self.sizer1.Add((-1,10)) #增加10个像素空白空间
        self.sizer1.Add(wx.Button(self,wx.ID_OK),0,wx.ALIGN_CENTER_HORIZONTAL|wx.TOP,border=20)
        self.SetSizer(self.sizer1)
        self.Center()