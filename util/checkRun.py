import os 
import psutil


'''
程序进程监控
'''

class checkRun():
    def __init__(self):
        self.path=os.getcwd()+"\\config\\"
        if not os.path.exists(self.path):
            os.mkdir(self.path)
        self.idFile=self.path+"pid"
        self.encoding="utf-8"
    
    def checkState(self):
        '''
        验证程序状态
        '''
        if os.path.exists(self.idFile):
            with open(self.idFile,'r',encoding=self.encoding) as read:
                    listTmp = read.readlines()
                    if len(listTmp)>0 and listTmp[0].isdigit():
                        idValue=int(listTmp[0])
                        if psutil.pid_exists(idValue) :
                            try:
                                name =psutil.Process(idValue)
                                if "wxedit" in name.name() or "python" in name.name():
                                    return True
                            except BaseException as exception:
                                print(exception)
                                return False

                        # result=os.system('start /B tasklist|find /i "'+listTmp[0]+'"')
                        # if result ==0:
                            # return True 
        else:
            return False
    
    def writeId(self):
        '''
        写入进程 
        '''
        with open(self.idFile,'w',encoding=self.encoding) as read:
            read.write(str(os.getpid()))