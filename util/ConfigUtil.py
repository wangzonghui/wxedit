'''
配置文件控制类
'''
import configparser,os


class ConfigUtil():
    def __init__(self):
        self.file=os.getcwd()+"\\config\\config.ini"
        self.fontSize=9
        self.backgroundColor='#c7edcc'
        self.encode='utf-8'
        self.config = configparser.ConfigParser()


    def read(self):
        '''
        读取内容
        '''
        if not os.path.exists(self.file):
            self.write(self.fontSize,self.backgroundColor)
            return self.fontSize,self.backgroundColor
        
        else:
            try:
                self.config.read(self.file,encoding=self.encode)
                return int(self.config.get("wxedit","fontSize")),self.config.get("wxedit","backgroundColor")
            except BaseException as exception:
                self.write(self.fontSize,self.backgroundColor)
                return self.fontSize,self.backgroundColor

    def changeColor(self,background):
        self.config.read(self.file)
        self.config.set("wxedit", "backgroundColor", background)

        with open(self.file,"w+",encoding=self.encode) as f:
            self.config.write(f)

    def changeSize(self,fontSize):
        self.config.read(self.file)
        self.config.set("wxedit", "fontSize", str(fontSize))

        with open(self.file,"w+",encoding=self.encode) as f:
            self.config.write(f)

    def write(self,fontSize,backgroundColor):
        '''
        写入内容
        '''
        self.config.add_section("wxedit")
        self.config.set("wxedit", "fontSize", str(self.fontSize))
        self.config.set("wxedit", "backgroundColor", self.backgroundColor)
        
        with open(self.file,"w",encoding=self.encode) as f:
            self.config.write(f)