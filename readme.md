[toc]
# 说明
本项目为wxPython开发文本编辑器项目，运行环境windows，从notepad++转Sublime text某些功能上，很不习惯，偶然机会决心开发个人文本编辑器，本项目由此诞生。

# 环境
python 3.9.9
wxPython 4.1.1 封装wxWidgets

# 资料
- 官方API[网址](https://docs.wxpython.org/)

# 安装
下载发行版，解压到安装目录，运行wxedit目录下wxedit.exe即可

# 介绍
- 本项目支持打开多个文件，本地文本或新建未保存文件，关闭窗口后内容不会丢失，即使突然断电或系统级关闭软件进程，内容依然保留。
- 工具栏红框分别为计算器、控制面板、mstsc和cmd，菜单栏附件中也有对应功能。
- 左下角为文件名和编码格式。
![](img/2022-01-20.png)

## 快捷键

|快捷键|说明|
|-|-|
|Ctrl+n|新建|
|Ctrl+d|删除行|
|Ctrl+l|复制行|
|Ctrl+p|拆分窗口|
|Ctrl+g|行合并|
|Ctrl+=|放大字体|
|Ctrl+-|缩小字体|

## 关闭文件
双击文件tab名关闭

